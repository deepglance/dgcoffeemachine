/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Design Studio.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import CoffeeMachine 1.0

import QtQuick.Controls 2.2
import DgQuick 1.0

ApplicationWindow {
    visible: true
    width: Constants.width
    height: Constants.height
    title: qsTr("Coffee")

    ApplicationFlow {
    }

    EyeTracker {
        model: EyeTracker.ModelTobii
        Component.onCompleted: start()
    }

    EventHandler {
        objectName: "eventHandler"
        id: eventHandler
        anchors.fill: parent
        focus: true
        Keys.onPressed: {
            if (event.key === Qt.Key_T) {
                trackstatus.visible = !trackstatus.visible;
                event.accepted = true;
            }
        }
    }

    ErrorHandler {
        objectName: "errorHandler"
        id: errorHandler
        anchors.fill: parent
        focus: true
        onError: {
            if (error === ErrorHandler.ErrorEyeTrackerSoftwareNotInstalled) {
                errorDialog.message = "Tobii software not installed"
                errorDialog.visible = true
            } else if (error === ErrorHandler.ErrorEyeTrackerNotConnected) {
                errorDialog.message = "Tobii eye tracker not connected"
                errorDialog.visible = true
            }
        }
    }

    TrackStatus {
        id: trackstatus
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        width: 100
        height: 60
        visible: false
    }

    Dialog {
        id: errorDialog
        property alias message: message.text
        title: "Error"
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: 300
        height: 150
        standardButtons: DialogButtonBox.Ok
        Text {
            id: message
        }
    }
}
